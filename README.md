**The Bee Game**
========================
Specification:
========================
Bees:
--------------
There are three types of bees in this game:
--------------
1. **Queen Bee** (1 bee):
    * Queen Bee has a lifespan of 100 Hit Points.
    * When the Queen Bee is hit, 8 Hit Points are deducted
from her lifespan.
    * If/When the Queen Bee has run out of Hit Points,
    * All remaining alive Bees automatically run out of hit
points.

2.  **Worker Bee**(5 bees):
    * Worker Bees have a lifespan of 75 Hit Points.
    * When a Worker Bee is hit, 10 Hit Points are deducted
from his lifespan.
3.  **Drone Bee**(8 bees):
    * Drone Bees have a lifespan of 50 Hit Points.
    * When a Drone Bee is hit, 12 Hit Points are deducted
from his lifespan.

Gameplay:
--------------

To play, there must be a button that enables a user to “hit” a random
bee. The selection of a bee must be random. When the bees are all
dead, the game must be able to reset itself with full life bees for
another round.
Constraints:
The application must run through a browser


Symfony Standard Edition
========================

Welcome to the Symfony Standard Edition - a fully-functional Symfony
application that you can use as the skeleton for your new applications.

For details on how to download and get started with Symfony, see the
[Installation][1] chapter of the Symfony Documentation.

What's inside?
--------------

The Symfony Standard Edition is configured with the following defaults:

  * An AppBundle you can use to start coding;

  * Twig as the only configured template engine;

  * Doctrine ORM/DBAL;

  * Swiftmailer;

  * Annotations enabled for everything.

It comes pre-configured with the following bundles:

  * **FrameworkBundle** - The core Symfony framework bundle

  * [**SensioFrameworkExtraBundle**][6] - Adds several enhancements, including
    template and routing annotation capability

  * [**DoctrineBundle**][7] - Adds support for the Doctrine ORM

  * [**TwigBundle**][8] - Adds support for the Twig templating engine

  * [**SecurityBundle**][9] - Adds security by integrating Symfony's security
    component

  * [**SwiftmailerBundle**][10] - Adds support for Swiftmailer, a library for
    sending emails

  * [**MonologBundle**][11] - Adds support for Monolog, a logging library

  * **WebProfilerBundle** (in dev/test env) - Adds profiling functionality and
    the web debug toolbar

  * **SensioDistributionBundle** (in dev/test env) - Adds functionality for
    configuring and working with Symfony distributions

  * [**SensioGeneratorBundle**][13] (in dev/test env) - Adds code generation
    capabilities

  * **DebugBundle** (in dev/test env) - Adds Debug and VarDumper component
    integration

All libraries and bundles included in the Symfony Standard Edition are
released under the MIT or BSD license.

Enjoy!

[1]:  https://symfony.com/doc/3.2/setup.html
[6]:  https://symfony.com/doc/current/bundles/SensioFrameworkExtraBundle/index.html
[7]:  https://symfony.com/doc/3.2/doctrine.html
[8]:  https://symfony.com/doc/3.2/templating.html
[9]:  https://symfony.com/doc/3.2/security.html
[10]: https://symfony.com/doc/3.2/email.html
[11]: https://symfony.com/doc/3.2/logging.html
[12]: https://symfony.com/doc/3.2/assetic/asset_management.html
[13]: https://symfony.com/doc/current/bundles/SensioGeneratorBundle/index.html