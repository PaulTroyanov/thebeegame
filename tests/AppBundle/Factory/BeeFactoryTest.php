<?php

namespace Tests\AppBundle\Factory;

use AppBundle\Factory\BeeFactory;
use PHPUnit\Framework\TestCase;

class BeeFactoryTest extends TestCase
{
    public function testGetQueen()
    {
        $beeFactory = new BeeFactory();
        $queen = $beeFactory->createBee("Queen Bee");
        $this->assertEquals("Queen Bee", $queen->getType());
    }

    public function testGetDrone()
    {
        $beeFactory = new BeeFactory();
        $drone = $beeFactory->createBee("Drone Bee");
        $this->assertEquals("Drone Bee", $drone->getType());
    }

    public function testGetWorker()
    {
        $beeFactory = new BeeFactory();
        $worker = $beeFactory->createBee("Worker Bee");
        $this->assertEquals("Worker Bee", $worker->getType());
    }
}