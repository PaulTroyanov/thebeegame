<?php

namespace AppBundle\Controller;

use AppBundle\Service\BeeService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $beeService = new BeeService();
        $bees = [];

        if ($request->get('hit')) {
            $beeService->hit($request);
            return $this->redirectToRoute("homepage");
        }

        $session = $request->getSession();
        
        if (!$session->get("Bees")) {
            $session->set("Bees", $beeService->newBeesRow());
            $session->set("Round", 1);
        }

        foreach ($session->get("Bees") as $bee) {
            $bees[] = unserialize($bee);
        }

        return $this->render('default/index.html.twig', ['bees' => $bees, 'round' => $session->get("Round")]);
    }




}
