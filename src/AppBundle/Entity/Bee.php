<?php

namespace AppBundle\Entity;

/**
 * Abstract Bee class which defines main characteristics of our bee
 *
 * Class Bee
 * @package AppBundle\Entity
 */
abstract class Bee
{
    /**
     * Type of Bee
     *
     * @var string
     */
    private $type;

    /**
     * Amount of damage when bee is hit
     *
     * @var int
     */
    private $damagePoints;

    /**
     * Amount of health points
     *
     * @var int
     */
    private $health;

    /**
     * @param $type string
     */
    public function setType($type) {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param $damagePoints int
     */
    public function setDamagePoints($damagePoints)
    {
        $this->damagePoints = $damagePoints;
    }

    /**
     * @return int
     */
    public function getDamagePoints()
    {
        return $this->damagePoints;
    }

    /**
     * @param $health int
     */
    public function setHealth($health)
    {
        $this->health = $health;
    }

    /**
     * @return int
     */
    public function getHealth()
    {
        return $this->health;
    }

    public function hit()
    {
        $this->health -= $this->damagePoints;
    }
}