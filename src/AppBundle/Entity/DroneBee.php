<?php

namespace AppBundle\Entity;


class DroneBee extends Bee
{
    public function __construct()
    {
        $this->setType("Drone Bee");
        $this->setDamagePoints(12);
        $this->setHealth(50);
    }
}