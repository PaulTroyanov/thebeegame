<?php

namespace AppBundle\Entity;


class WorkerBee extends Bee
{
    public function __construct()
    {
        $this->setType("Worker Bee");
        $this->setDamagePoints(10);
        $this->setHealth(75);
    }
}