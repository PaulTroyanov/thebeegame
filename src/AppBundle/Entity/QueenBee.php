<?php

namespace AppBundle\Entity;


class QueenBee extends Bee
{
    public function __construct()
    {
        $this->setType("Queen Bee");
        $this->setDamagePoints(8);
        $this->setHealth(100);
    }
}