<?php

namespace AppBundle\Factory;

use AppBundle\Entity;

/**
 * It's a simple factory class
 *
 * Class BeeFactory
 * @package AppBundle\Factory
 */
class BeeFactory
{
    /**
     * @param $beeType string
     * @return Entity\DroneBee|Entity\QueenBee|Entity\WorkerBee|null
     */
    public function createBee($beeType) {
        switch ($beeType) {
            case "Queen Bee":
                return new Entity\QueenBee();
                break;

            case "Worker Bee":
                return new Entity\WorkerBee();
                break;

            case "Drone Bee":
                return new Entity\DroneBee();
                break;
        }
        return null;
    }
}