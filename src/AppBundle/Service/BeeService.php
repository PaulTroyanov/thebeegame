<?php

namespace AppBundle\Service;

use AppBundle\Factory\BeeFactory;
use Symfony\Component\HttpFoundation\Request;

class BeeService
{

    const NUMBER_OF_QUEENS = 1;
    const NUMBER_OF_WORKERS = 5;
    const NUMBER_OF_DRONES = 8;

    public function hit(Request $request)
    {
        $bees = [];
        $serializedBees = [];
        $session = $request->getSession();

        foreach ($session->get("Bees") as $bee) {
            $bees[] = unserialize($bee);
        }

        $hitNumber = rand(0, count($bees) - 1);
        $bees[$hitNumber]->hit();

        if($bees[$hitNumber]->getHealth() <= 0) {
            if ($bees[$hitNumber]->getType() == "Queen Bee" || count($bees) == 1) {
                $session->set("Bees", $this->newBeesRow());
                $round = $session->get("Round");
                $session->set("Round", ++$round);

                return true;
            }
            unset($bees[$hitNumber]);
        }

        foreach ($bees as $bee) {
            $serializedBees[] = serialize($bee);
        }

        $session->set("Bees", $serializedBees);
        return true;
    }

    public function newBeesRow()
    {
        $bees = [];
        $beeFactory = new BeeFactory();

        for ($i = 0; $i < self::NUMBER_OF_QUEENS; ++$i) {
            $bees[] = serialize($beeFactory->createBee("Queen Bee"));
        }

        for ($i = 0; $i < self::NUMBER_OF_WORKERS; ++$i) {
            $bees[] = serialize($beeFactory->createBee("Worker Bee"));
        }

        for ($i = 0; $i < self::NUMBER_OF_DRONES; ++$i) {
            $bees[] = serialize($beeFactory->createBee("Drone Bee"));
        }

        return $bees;
    }
}